
var SystemPartitionContent = function(id)
{
  var systemPartitionContent = this;
  
  systemPartitionContent.id = id || 'system_partition';
  
  //------------------------------------------------------------------------------
  // internal functions
  //------------------------------------------------------------------------------

  //------------------------------------------------------------------------------
  // initialize view elements
  //------------------------------------------------------------------------------
  var CreateSystemPartitionContentParamView = (function()
  {
    systemPartitionContent.paramView = new DeviceParamView();
            
    // no output handling specified, because output must consider other coupler values
    systemPartitionContent.paramView.Add(
    {
      paramId         : 'active_partition'
    });
        
    systemPartitionContent.paramView.Add(
    {
      paramId         : 'active_partition_medium'
    });
    
  })();

  
  //------------------------------------------------------------------------------
  // event - first creation of area
  //------------------------------------------------------------------------------
  $('#content_area').bind(systemPartitionContent.id + '_create', function(event)
  {
    $('#' + systemPartitionContent.id + '_content #start_backup_system_button').bind('click', function(event)
    {
      event.preventDefault();
      systemPartitionContent.SwitchBootloader();
    });
  });


  //------------------------------------------------------------------------------
  // event - refresh content area
  //------------------------------------------------------------------------------
  $('#content_area').bind(systemPartitionContent.id + '_refresh', function(event)
  {
    systemPartitionContent.Refresh();
  });

};


SystemPartitionContent.prototype.Refresh = function()
{
  var systemPartitionContent = this;
  
  deviceParams.ReadValueGroup(systemPartitionContent.paramView.list, function()
  {
    //systemPartitionContent.paramView.ShowValues();
    
    if(deviceParams.ReadErrorOccured(systemPartitionContent.paramView.list)) 
    {
      var errorString = deviceParams.CollectedErrorStrings(systemPartitionContent.paramView.list);
      $('body').trigger('event_errorOccured', [ 'Error while reading filesystem data.', errorString ] );
    }
    else
    {
      var activePartitionMedium = deviceParams.list['active_partition_medium'].value;
      var activePartition       = deviceParams.list['active_partition'].value;
      
      // filter partition number from active partition string and correct it depending on active medium
      var partitionNo           = parseInt(activePartition.charAt(activePartition.length - 1), 10);
      
      if('sd-card' === activePartitionMedium)
      {
        partitionNo -= 1;
      }
      else if('internal-flash-nand' === activePartitionMedium)
      {
        partitionNo += 1;
      }

      $('#' + systemPartitionContent.id + '_content .active_partition').html('Partition ' + partitionNo);
      $('#' + systemPartitionContent.id + '_content .active_partition_medium').html('on ' + DeviceMediumOutputText(activePartitionMedium))
    }
  });

};


SystemPartitionContent.prototype.SwitchBootloader = function() 
{
  // as a precaution recheck
  if(confirm('Really want to switch boot partition?'))
  {
    pageElements.ShowBusyScreen('Switch bootloader...');
 
    var newValueList = { };

    deviceParams.ChangeValue('switch_bootloader', newValueList, function(status, errorText)
    {
      pageElements.RemoveBusyScreen();
      if(SUCCESS != status)
      {
        $('body').trigger('event_errorOccured', [ 'Error while switching bootloader.', errorText ]);
      }
        
    });
  }
};

