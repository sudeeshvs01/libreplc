/*
 * functions for read and write settings of DataAgent
 */

 
 
/*---------------------------------------------------------------------------
* define parameters
* ---------------------------------------------------------------------------
*/
var CreateIEC60870Params = (function()
{
    deviceParams.Add(
    {
      id                    : 'dataagent_operation_status',
      exampleValue          : '{...}' ,
      configtoolReadParams  :
      {
        name      : 'get_dataagent_config',
        parameter : [ 'dataagent-operation-status' ]
      }
    });

    deviceParams.Add(
    {
      id                    : 'dataagent_config_params',
      exampleValue          : '{...}' ,
      configtoolReadParams  :
      {
        name      : 'get_dataagent_config',
        parameter : [ 'dataagent-config-params' ]
      }
    });
	
	deviceParams.Add(
	{
	  id                    : 'dataagent_feature',
	  invariant             : true,
	  configtoolReadParams:
	  {
	   name      : 'detectfeature',
	   parameter : [ 'feature_dataagent' ]
	  }
	});

	deviceParams.Add(
	{
	  id                    : 'iec60870_',
	  configtoolWriteParams :
	  {
		name      : 'config_dataagent',
		parameter : [ 
					  'theNewIoTPlatform=$theNewIoTPlatform',
					  'theNewHostName=$theNewHostName',
					  'theNewDeviceId=$theNewDeviceId',
					  'theNewSharedAccessKey=$theNewSharedAccessKey',
					  'theMqttHost=$theMqttHost',
					  'theMqttUser=$theMqttUser',
					  'theMqttPassword=$theMqttPassword',
					  'theMqttClientId=$theMqttClientId',
					  'theMqttTLSOn=$theMqttTLSOn',
					  'theMqttPort=$theMqttPort',
					  'theMqttCaFile=$theMqttCaFile',
					  'theMqttCertFile=$theMqttCertFile',
					  'theMqttKeyFile=$theMqttKeyFile',
					  'theCacheMode=$theCacheMode',
					  'theDeviceInfoEnabled=$theDeviceInfoEnabled',
					  'theDeviceStatusEnabled=$theDeviceStatusEnabled',
                      'theStandardCommandsEnabled=$theStandardCommandsEnabled',
					],
		sudo      : true
	  }
	});

})();

  
function DataAgentPageContent(id)
{
  var thePageContent = this;
  thePageContent.id = id;
  
  var RefreshVisibility = function()
  {
	var iotplatformValue = $("#selectIotPlatform").val();
	//console.log(iotplatformValue);
	switch (iotplatformValue)
	{
		case "ECloud":
			$("#AzureIoTconfigUI").show();
			$('#mqttIoTconfigUI').hide();
			$('#IBMConfigUI').hide();
			$('#AWSConfigUI').hide();
			$('#DeviceConfigUI').hide();
			$('#CommandsConfigUI').hide();
            $("#chkDeviceInfoEnabled").prop('checked', 'true');
            $("#chkDeviceStatusEnabled").prop('checked', 'true');
            $("#chkStandardCommandsEnabled").prop('checked', 'true');
            $("#chkMqttSecurityEnabled").removeAttr('disabled');
            $("#inputMqttPort").removeAttr('disabled');
			var varHostName = $('#inputHostName').val();
			if (!varHostName)
			{
				$("#inputHostName").val('wagocloud.azure-devices.net');
			}
			
			break;
		case "AzureCloud":
			$("#AzureIoTconfigUI").show();
			$("#mqttIoTconfigUI").hide();
			$('#IBMConfigUI').hide();
			$('#AWSConfigUI').hide();
			$('#DeviceConfigUI').show();
			$('#CommandsConfigUI').show();
            $("#chkMqttSecurityEnabled").removeAttr('disabled');
            $("#inputMqttPort").removeAttr('disabled');
			break;
		case "MQTT":
			$("#AzureIoTconfigUI").hide();
			$("#mqttIoTconfigUI").show();
			$('#IBMConfigUI').show();
			$('#AWSConfigUI').show();
			$('#DeviceConfigUI').show();
			$('#CommandsConfigUI').show();
            $("#chkMqttSecurityEnabled").removeAttr('disabled');
            $("#inputMqttPort").removeAttr('disabled');
			break;
		case "IBM":
			$("#AzureIoTconfigUI").hide();
			$("#mqttIoTconfigUI").show();
			$('#IBMConfigUI').show();
			$('#AWSConfigUI').hide();
			$('#DeviceConfigUI').show();
			$('#CommandsConfigUI').hide();
	    	$("#chkStandardCommandsEnabled").prop('checked', false);
            $("#chkMqttSecurityEnabled").removeAttr('disabled');
            $("#inputMqttPort").removeAttr('disabled');
			break;
		case "AWS":
			$("#AzureIoTconfigUI").hide();
			$("#mqttIoTconfigUI").show();
			$('#IBMConfigUI').hide();
			$('#AWSConfigUI').show();
			$('#DeviceConfigUI').show();
			$('#CommandsConfigUI').show();
            $("#chkMqttSecurityEnabled").prop('checked', 'true');
            $("#chkMqttSecurityEnabled").attr('disabled', 'true');
            $("#inputMqttPort").val('8883');
            $("#inputMqttPort").attr('disabled','true');
			break;
		default:
			$("#AzureIoTconfigUI").hide();
			$("#mqttIoTconfigUI").hide();
			$('#IBMConfigUI').hide();
			$('#AWSConfigUI').hide();
			$('#DeviceConfigUI').hide();
			$('#CommandsConfigUI').hide();
	}
  };
  
    var DisplayConfigParams = function(paramStatus, paramValue, outputElementId)
    {
        //console.log('' + outputElementId  + ' ' + paramStatus + ' ' + paramValue);
        try
        {
            configParams = jQuery.parseJSON(paramValue);
            var parentElemIdPrefix = "#" + thePageContent.id + "_content ";
            $(parentElemIdPrefix + "#selectIotPlatform").prop('value', configParams.IoTPlatform); // IE-8 compatible syntax

            $(parentElemIdPrefix + "#inputHostName").val(configParams.HostName);
            $(parentElemIdPrefix + "#inputDeviceId").val(configParams.DeviceId);
            $(parentElemIdPrefix + "#inputSharedAccessKey").val(configParams.SharedAccessKey);

            $(parentElemIdPrefix + "#inputMqttHostName").val(configParams.MQTTHost);
            $(parentElemIdPrefix + "#inputMqttUserName").val(configParams.MQTTUser);
            $(parentElemIdPrefix + "#inputMqttUserPassword").val(configParams.MQTTPassword);
            $(parentElemIdPrefix + "#inputMqttClientId").val(configParams.MQTTClientId);
            $(parentElemIdPrefix + "#chkMqttSecurityEnabled").prop('checked', configParams.MQTTTLSOn);
            $(parentElemIdPrefix + "#inputMqttPort").val(configParams.MQTTPort);
            $(parentElemIdPrefix + "#inputMqttCaFile").val(configParams.MQTTCaFile);
            $(parentElemIdPrefix + "#inputMqttCertFile").val(configParams.MQTTCertFile);
            $(parentElemIdPrefix + "#inputMqttKeyFile").val(configParams.MQTTKeyFile);

            $(parentElemIdPrefix + "#chkDeviceInfoEnabled").prop('checked', configParams.DeviceInfo);
            $(parentElemIdPrefix + "#chkDeviceStatusEnabled").prop('checked', configParams.DeviceStatus);
            $(parentElemIdPrefix + "#chkStandardCommandsEnabled").prop('checked', configParams.StandardCommandsEnabled);

            RefreshVisibility();

            if (configParams.NonVolatileStorageIsAvailable == false)
            {
                $(parentElemIdPrefix + "#selectCacheMode").prop('value', 'RAM');
                $(parentElemIdPrefix + "#selectCacheMode").attr('disabled', 'true');
            }
            else
            {
                $(parentElemIdPrefix + "#selectCacheMode").prop('value', configParams.CacheMode); // IE-8 compatible syntax
                $(parentElemIdPrefix + "#selectCacheMode").removeAttr('disabled');
            }
        }
        catch(e)
        {
            console.log('Error while parsing DataAgent configuration from a JSON string!');
        }
    }

    var DisplayDataAgentStopped = function()
    {
		var parentElemIdPrefix = "#" + thePageContent.id + "_content ";
		
		var operationSpanElem = $(parentElemIdPrefix + "#spanOperation");
		operationSpanElem.empty();
		operationSpanElem.text("stopped");
		var redCrossImage = $('<img class="state" src="images/kreuz_rot_10.gif" />', { 
		 title: "Application not running",
		 alt: "stopped"
		});
		redCrossImage.prependTo(operationSpanElem);
		
        $(parentElemIdPrefix + "#spanCollectionsCount").text("");
		$(parentElemIdPrefix + "#spanConnection").text("");
        $(parentElemIdPrefix + "#spanHeartbeat").text("");
        $(parentElemIdPrefix + "#spanTelemetryTransmission").text("");
        $(parentElemIdPrefix + "#spanFillLevel").text("");
    }

    var DisplayLiveStatus = function(paramStatus, paramValue, outputElementId)
    {
        //console.log('' + outputElementId + ' ' + paramValue + ' ' + paramStatus);
        try
        {
            value = jQuery.parseJSON(paramValue);

            var parentElemIdPrefix = "#" + thePageContent.id + "_content ";
            var previosValue = "" + $(parentElemIdPrefix + "#theSignOfLifeHidden").val();

            // We can reliebly compare because javascript timer interval is greater than interval producing the status
            if (previosValue != value.SignOfLife)
            {
                // Sign of life changed, so we know DataAgent is alive
                $(parentElemIdPrefix + "#theSignOfLifeHidden").val(value.SignOfLife);
				
				var greenCheckImage = $('<img class="state" src="images/haken_gruen_10.gif" />');
				var redCrossImage = $('<img class="state" src="images/kreuz_rot_10.gif" />');
				
				
				var operationSpanElem = $(parentElemIdPrefix + "#spanOperation");
				operationSpanElem.empty();
				operationSpanElem.text("running");
				greenCheckImage.clone().prop('title', 'running').prop('alt', 'running').prependTo(operationSpanElem);
				
				
                $(parentElemIdPrefix + "#spanCollectionsCount").text('' + value.CollectionsCountFromPlc);
				$(parentElemIdPrefix + "#spanHeartbeat").text('' + value.Heartbeat + ' seconds');
				
				
				var connectionSpanElem = $(parentElemIdPrefix + "#spanConnection");
				connectionSpanElem.empty();
                if (value.Connected)
                {
					connectionSpanElem.text("connected");
                    greenCheckImage.clone().prop('title', 'connected').prop('alt', 'connected').prependTo(connectionSpanElem);
                }
                else
                {
					connectionSpanElem.text("disconnected");
                    redCrossImage.clone().prop('title', 'disconnected').prop('alt', 'disconnected').prependTo(connectionSpanElem);
                }

				
				var transmissionSpanElem = $(parentElemIdPrefix + "#spanTelemetryTransmission");
				transmissionSpanElem.empty();
                if (value.TelemetryDataTransmission)
                {
					transmissionSpanElem.text("enabled");
                    greenCheckImage.clone().prop('title', 'enabled').prop('alt', 'enabled').prependTo(transmissionSpanElem);
                }
                else
                {
					transmissionSpanElem.text("disabled");
                    redCrossImage.clone().prop('title', 'disabled').prop('alt', 'disabled').prependTo(transmissionSpanElem);
                }
				
				
                var fillLevelAsFloat = parseFloat(value.FillLevel);
                var fillLevelRoundedNumber = (Number(fillLevelAsFloat)).toFixed(2);
                $(parentElemIdPrefix + "#spanFillLevel").text('' + fillLevelRoundedNumber + ' %');
            }
            else
            {
                DisplayDataAgentStopped();
            }
        }
        catch(e)
        {
            //console.log('Error while parsing live status of DataAgent from a JSON string!');
            DisplayDataAgentStopped();
        }
    };


    var RefreshStatusDataAgent = function()
    {
        //console.log('DataAgentPageContent.prototype.RefreshStatusDataAgent');
        deviceParams.ReadValueGroup(thePageContent.StatusParamView.list, true, function()
        {
            thePageContent.StatusParamView.ShowValues();
            if(deviceParams.ReadErrorOccured(thePageContent.StatusParamView.list))
            {
                var errorString = deviceParams.CollectedErrorStrings(thePageContent.StatusParamView.list);
                $('body').trigger('event_errorOccured', [ 'Error while reading DataAgent status.', errorString ] );
            };
        });
    }


  /*
   * definitions for display of parameters
   */
  var CreateIEC60870ParamView = (function()
  {
    thePageContent.paramView = new DeviceParamView();

    thePageContent.paramView.Add(
    {
        paramId         : 'dataagent_config_params',
        outputElementId : thePageContent.id + '_content #dom_dataagent_config_params',
        outputFkt       : DisplayConfigParams
    });

    var dataAgentStatusParameter = {
        paramId         : 'dataagent_operation_status',
        outputElementId : thePageContent.id + '_content #dom_dataagent_operation_status',
        outputFkt       : DisplayLiveStatus
    };

    thePageContent.paramView.Add(dataAgentStatusParameter);

    // View instance for status of DataAgent
    thePageContent.StatusParamView = new DeviceParamView();
    thePageContent.StatusParamView.Add(dataAgentStatusParameter);

	//console.log('CreateIEC60870ParamView end');
  })();
 
 
    // Register for create event: It will be fired after user clicked IoT Connectivity the very first time within the navigation pane
    $('#content_area').bind(thePageContent.id + '_create', function(event)
    {
        $( "#selectIotPlatform" ).change(function() {
            RefreshVisibility();
        });

        $("#iec60870_104_form").bind('submit', function(event)
        {
            event.preventDefault();
            thePageContent.ChangeSettingsofDataAgent(this);
        });
    });

  
    // Register for refresh event: It will be fired after user clicked IoT Connectivity within the navigation pane
    $('#content_area').bind(thePageContent.id + '_refresh', function(event)
    {
        thePageContent.myIntervalTimerId = window.setInterval(RefreshStatusDataAgent, 31 * 1000);
        thePageContent.Refresh();
    });

    // Register for sleep event: It will be fired after user clicked other pane than IoT Connectivity within the navigation pane
    $('#content_area').bind('event_' + thePageContent.id + '_sleep', function(event)
    {
        window.clearInterval(thePageContent.myIntervalTimerId);
    });

  //console.log('DataAgentPageContent end');
};


  
/*
 * read and show parameters necessary for DataAgent content
 */
DataAgentPageContent.prototype.Refresh = function()
{
  var pageContent = this;

  deviceParams.ReadValueGroup(pageContent.paramView.list, function()
  {
    pageContent.paramView.ShowValues();

    if(deviceParams.ReadErrorOccured(pageContent.paramView.list)) 
    {
      var errorString = deviceParams.CollectedErrorStrings(pageContent.paramView.list);
      $('body').trigger('event_errorOccured', [ 'Error while reading DataAgent settings.', errorString ] );
    };
  });
  
  //console.log('DataAgentPageContent.prototype.Refresh end');
};


/*
 * functions for changing DataAgent configuration
 */
DataAgentPageContent.prototype.ChangeSettingsofDataAgent = function(formObj)
{
  pageElements.ShowBusyScreen("Changing DataAgent settings ...");
  
  // get script parameter from form data - given, but empty strings must be converted to "" to let the script
  // know, that this parameter value should be deleted.
  var platform = $(formObj).find('#selectIotPlatform').val()  || "\"\"";
  var varHostName = $(formObj).find('input#inputHostName').val()  || "\"\"";
  var varDeviceId = $(formObj).find('input#inputDeviceId').val()  || "\"\"";
  var varSharedAccessKey = $(formObj).find('input#inputSharedAccessKey').val()  || "\"\"";
  
  var varMqttHost = $(formObj).find('#inputMqttHostName').val()  || "\"\"";
  var varMqttUser = $(formObj).find('#inputMqttUserName').val()  || "\"\"";
  var varMqttPassword = $(formObj).find('#inputMqttUserPassword').val()  || "\"\"";
  var varMqttClientId = $(formObj).find('#inputMqttClientId').val()  || "\"\"";
  var varMqttSecurityEnabled = $(formObj).find('#chkMqttSecurityEnabled').is(':checked')  || "\"\"";
  var varMqttPort = $(formObj).find('#inputMqttPort').val()  || "\"\"";
  var varMqttCaFile = $(formObj).find('#inputMqttCaFile').val()  || "\"\"";
  var varMqttCertFile = $(formObj).find('#inputMqttCertFile').val()  || "\"\"";
  var varMqttKeyFile = $(formObj).find('#inputMqttKeyFile').val()  || "\"\"";

  var varCacheMode = $(formObj).find('#selectCacheMode').val()  || "\"\"";

  var varDeviceInfoEnabled = $(formObj).find('#chkDeviceInfoEnabled').is(':checked')  || "\"\"";
  var varDeviceStatusEnabled = $(formObj).find('#chkDeviceStatusEnabled').is(':checked')  || "\"\"";
  var varStandardCommandsEnabled = $(formObj).find('#chkStandardCommandsEnabled').is(':checked');

  
  //console.log('varMqttSecurityEnabled ' + varMqttSecurityEnabled);

  var newValueList  = {
						theNewIoTPlatform: platform,
						theNewHostName: varHostName,
						theNewDeviceId: varDeviceId,
						theNewSharedAccessKey: varSharedAccessKey,
						theMqttHost: varMqttHost,
						theMqttUser: varMqttUser,
						theMqttPassword: varMqttPassword,
						theMqttClientId: varMqttClientId,
						theMqttTLSOn: varMqttSecurityEnabled,
						theMqttPort: varMqttPort,
						theMqttCaFile: varMqttCaFile,
						theMqttCertFile: varMqttCertFile,
						theMqttKeyFile: varMqttKeyFile,
						theCacheMode: varCacheMode,
						theDeviceInfoEnabled: varDeviceInfoEnabled,
                        theDeviceStatusEnabled: varDeviceStatusEnabled,
                        theStandardCommandsEnabled: varStandardCommandsEnabled
					  };

  deviceParams.ChangeValue('iec60870_', newValueList, function(status, errorText)
  {
    pageElements.RemoveBusyScreen();
    if(SUCCESS != status)
    {
      $('body').trigger('event_errorOccured', [ 'Error while changing DataAgent settings.', errorText ]);
    }
	else
	{
		$('#rebootHint').css('color','orangered').css('font-weight', 'bold');
	}
  });
};
