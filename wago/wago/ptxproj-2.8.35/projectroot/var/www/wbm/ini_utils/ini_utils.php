<?php namespace ini_utils;

function max_post_size() {
    $maxPostSize = ini_get('post_max_size');
    if (preg_match('/K$/', $maxPostSize)) {
        $maxPostSize = intval($maxPostSize) * 1024;
    } else if (preg_match('/M$/', $maxPostSize)) {
        $maxPostSize = intval($maxPostSize) * 1024 * 1024;
    } else if (preg_match('/G$/', $maxPostSize)) {
        $maxPostSize = intval($maxPostSize) * 1024 * 1024 * 1024;
    }
    return $maxPostSize;
}
