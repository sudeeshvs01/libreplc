kbd-1.15.2/AUTHORS

The programs loadkeys, dumpkeys and showkey were derived from
Risto Kankkunen's kbd-0.81.

The font loading code goes back to the codepage package by
Joel Hoffman (joel@wam.umd.edu). (He tells me that the original
reference is: "From: p. 307 of _Programmer's Guide to PC & PS/2
Video Systems_ by Richard Wilton. 1987.  Microsoft Press".)

The kernel and user interface for the font loading were derived
from Eugene Crosser's intl_con package.

[Similar things were done earlier by
 Jerry Kaidor (tr2!jerry@abekas.com, jkaidor@synoptics.com) : fontpak,
 H. Peter Anvin (hpa@nwu.edu) and Pavel Zaboj (xzaboj@vse.cs).
Their code was not used, but the Latin-1 *.psf fonts were developed
by H. Peter Anvin, and the Latin-2 ones by Pavel Zaboj.]


kbd-1.15.2/COPYING

The file
	kbdrate.c
is Copyright (C) 1992 Rickard E. Faith.

The files
	dumpkeys.c loadkeys.y analyze.l ksyms.c ksyms.h
	showkey.c keymaps.5 loadkeys.1 dumpkeys.1 showkey.1
are Copyright (C) 1993 Risto Kankkunen.

The files
	setfont.c setfont.8 mapscrn.c mapscrn.8
(and changes to loadkeys.y)
are Copyright (C) 1993 Eugene G. Crosser.

The files
	psfaddtable.1 psfgettable.1 psfstriptable.1
are Copyright (C) 1994 H. Peter Anvin.

The files
	chvt.c clrunimap.c deallocvt.c fgconsole.c findfile.c
	getfd.c getkeycodes.c getunimap.c kbd_mode.c loadunimap.c
	psffontop.c psfxtable.c resizecons.c screendump.c setkeycodes.c
	setleds.c setmetamode.c setvesablank.c showconsolefont.c
	spawn_console.c spawn_login.c
	chvt.1 deallocvt.1 kbd_mode.1 setleds.1 setmetamode.1
	getkeycodes.8 loadunimap.8 resizecons.8 setkeycodes.8
	kbd.FAQ.*
(and changes to earlier mentioned programs)
are Copyright (C) 1994-1999 Andries E. Brouwer.

All files in this package may be freely copied under the terms
of the GNU General Public License (GPL), version 2, or at your
option any later version - except possibly for the restrictions
mentioned in the directory consolefonts.

