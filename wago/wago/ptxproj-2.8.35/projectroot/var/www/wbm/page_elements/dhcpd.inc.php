<?php include("page_element_general_content.inc.php"); ?>

    <h2>DHCP Configuration</h2>
  
    <p>Changes will take effect immediately.</p>
    <p>Example values for one static host: "01:02:03:04:05:06=192.168.1.20" or "hostname=192.168.1.20".</p>
    
    <div id="dhcpd_configuration_area"></div>
    
