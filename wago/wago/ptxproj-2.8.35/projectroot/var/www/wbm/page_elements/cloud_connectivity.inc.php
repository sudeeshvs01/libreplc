<h2>Configuration of Cloud Connectivity</h2>
<p id="rebootHint">Changes will take effect after next reboot.</p>
<input type="hidden" name="signOfLifeHidden" id="theSignOfLifeHidden" value="" />
<form id="iec60870_104_form" action="javascript_requested.php" method="POST">

<h3>Software version</h3>
<div class="config_form_box">
  <div class="config_data_list"
	<div class="label_value_pair">
	  <!-- TODO:ReleaseBranch. On Trunk, we want show SVN Revision number -->
	  <div class="label_field"><label for="attention">1.0.1</label></div>
	</div>
</div>

<h3>Status</h3>
<div class="config_form_box">
	<div class="config_data_list">
		<div class="label_value_pair">
		  <div class="label_field">Operation:</div>
		  <div class="value_field"><span id="spanOperation">updating... please wait</span></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field">Data collections:</div>
		  <div class="value_field"><span id="spanCollectionsCount">updating... please wait</span></div>
		</div>
	  <div class="label_value_pair">
		<div class="label_field">Connection:</div>
		<div class="value_field"><span id="spanConnection">updating... please wait</span></div>
	  </div>
	  <div class="label_value_pair">
		<div class="label_field">Heartbeat:</div>
		<div class="value_field"><span id="spanHeartbeat">updating... please wait</span></div>
	  </div>
	  <div class="label_value_pair">
		<div class="label_field">Telemetry data transmission:</div>
		<div class="value_field"><span id="spanTelemetryTransmission">updating... please wait</span></div>
	  </div>
	  <div class="label_value_pair">
		<div class="label_field">Cache fill level:</div>
		<div class="value_field"><span id="spanFillLevel">updating... please wait</span></div>
	  </div>
	</div>
</div>

    <h3>Settings</h3>
    <div class="config_form_box">
  
	  <div class="config_data_list">
		<div class="label_value_pair">
		  <div class="label_field"><label for="selectIotPlatform">Cloud platform:</label></div>
			<div class="value_field">
			  <select id="selectIotPlatform" class="wide" name="selectIotPlatform" size="1">
				<option value="ECloud">Wago Cloud</option>
				<option value="AzureCloud">Azure</option>
				<option value="MQTT">MQTT AnyCloud</option>
				<option value="IBM">IBM Bluemix</option>
				<option value="AWS">Amazon Web Services (AWS)</option>
				<option value="Unknown" selected>Please select...</option>
			  </select>
			</div>
		</div>
	  </div>

	  <div class="config_data_list" id="AzureIoTconfigUI" style="display: none;"> 
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputHostName">Hostname:</label></div>
		  <div class="value_field"><input id="inputHostName" class="input_field" type="text" name="inputHostName" size="50" maxlength="250" value="" ></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputDeviceId">Device ID:</label></div>
		  <div class="value_field"><input id="inputDeviceId" class="input_field" type="text" name="DeviceId" size="50" maxlength="250" value="" ></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputSharedAccessKey">Activation Key:</label></div>
		  <div class="value_field"><input id="inputSharedAccessKey" class="input_field" type="text" name="inputSharedAccessKey" size="50" maxlength="250" value="" ></div>
		</div>
	  </div>

	  <div class="config_data_list" id="mqttIoTconfigUI" style="display: none;"> 
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputMqttHostName">Hostname:</label></div>
		  <div class="value_field"><input id="inputMqttHostName" class="input_field" type="text" name="inputMqttHostName" size="50" maxlength="250" value="" ></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputMqttClientId">Device ID:</label></div>
		  <div class="value_field"><input id="inputMqttClientId" class="input_field" type="text" name="inputMqttClientId" size="50" maxlength="300" value="" ></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field"><label for="chkMqttSecurityEnabled">TLS:</label></div>
		  <div class="value_field"><input id="chkMqttSecurityEnabled" type="checkbox" name="chkMqttSecurityEnabled" size="50" maxlength="300" value="enabled" ></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputMqttPort">Port:</label></div>
		  <div class="value_field"><input id="inputMqttPort" class="input_field" type="text" name="inputMqttPort" size="50" maxlength="300" value="" ></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputMqttCaFile">CA file:</label></div>
		  <div class="value_field"><input id="inputMqttCaFile" class="input_field" type="text" size="50" maxlength="250" name="inputMqttCaFile" value="" ></div>
		</div>
	  </div>

	  <div class="config_data_list" id="IBMConfigUI" style="display: none;"> 
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputMqttUserName">User:</label></div>
		  <div class="value_field"><input id="inputMqttUserName" class="input_field" type="text" name="inputMqttUserName" size="50" maxlength="250" value="" ></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputMqttUserPassword">Password:</label></div>
		  <div class="value_field"><input id="inputMqttUserPassword" class="input_field" type="password" name="inputMqttUserPassword" size="50" maxlength="250" value="" ></div>
		</div>
	  </div>

	  <div class="config_data_list" id="AWSConfigUI" style="display: none;"> 
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputMqttCertFile">Certification file:</label></div>
		  <div class="value_field"><input id="inputMqttCertFile" class="input_field" type="text" size="50" maxlength="250" name="inputMqttCertFile" value="" ></div>
		</div>
		<div class="label_value_pair">
		  <div class="label_field"><label for="inputMqttKeyFile">Key file:</label></div>
		  <div class="value_field"><input id="inputMqttKeyFile" class="input_field" type="text" size="50" maxlength="250" name="inputMqttKeyFile" value="" ></div>
		</div>
	  </div>
  
      <div class="config_data_list">
        <div class="label_value_pair">
          <div class="label_field"><label for="selectCacheMode">Cache mode:</label></div>
          <div class="value_field">
            <select id="selectCacheMode" class="wide" name="selectCacheMode" size="1">
              <option value="RAM">RAM (volatile)</option>
              <option value="SDCard" >SD Card (non-volatile)</option>
              <option value="Unknown" selected>Please select...</option>
            </select>
          </div>
        </div>
      </div>
	  
      <div class="config_data_list" id="DeviceConfigUI" style="display: none;"> 
        <div class="label_value_pair">
          <div class="label_field"><label for="chkDeviceInfoEnabled">Device info:</label></div>
          <div class="value_field"><input id="chkDeviceInfoEnabled" type="checkbox" name="chkDeviceInfoEnabled" value="enabled" ></div>
        </div>
        <div class="label_value_pair">
          <div class="label_field"><label for="chkDeviceStatusEnabled">Device status:</label></div>
          <div class="value_field"><input id="chkDeviceStatusEnabled" type="checkbox" name="chkDeviceStatusEnabled" value="enabled" ></div>
        </div>
      </div>

      <div class="config_data_list" id="CommandsConfigUI" style="display: none;"> 
        <div class="label_value_pair">
          <div class="label_field"><label for="chkStandardCommandsEnabled">Standard commands:</label></div>
          <div class="value_field"><input id="chkStandardCommandsEnabled" type="checkbox" name="chkStandardCommandsEnabled" value="enabled" ></div>
        </div>
      </div>
	  
      <p class="button_area">
        <input class="button" type="SUBMIT" value="Submit" name="Submit">          
      </p>
	  
    </div>
</form>	
      
<?php include("page_element_general_content.inc.php"); ?>
