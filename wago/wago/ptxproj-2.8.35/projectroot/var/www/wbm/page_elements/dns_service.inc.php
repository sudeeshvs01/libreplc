<?php include("page_element_general_content.inc.php"); ?>

    <h2>Configuration of DNS Service</h2>
	  
    <p>Changes will take effect immediately.</p>
    <p>Example values for one static host: "192.168.1.20:hostname".</p>
    
    <h3>DNS Service</h3>
    
    <div id="dns_service_configuration_area"></div>
    
