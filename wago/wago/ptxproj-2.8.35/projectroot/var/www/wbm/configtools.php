<?php

include("session_lifetime.inc.php");
//define("MOCK", true);
define("MOCK", false);
define("USERLEVEL_GUEST", 0);
define("USERLEVEL_USER",  2);
define("USERLEVEL_ADMIN", 3);


function MockConfigtool($callString, &$dummy, &$status)
{
  $status       = 0;
  $resultString = "";

  switch($callString)
  {
    // read tools

    case "/etc/config-tools/get_coupler_details order-number":

      $resultString = "0758-0874-0000-0111";
      break;

    case "/etc/config-tools/get_actual_eth_config eth0 ip-address":

      $resultString = "192.168.1.17";
      break;

    // write tools

    case "/etc/config-tools/change_hostname hostname=test":

      $resultString = "";
      break;

    case "/etc/config-tools/change_hostname hostname=te+st":

      $resultString = "";
      break;

    case "/etc/config-tools/config_interfaces interface=eth0 ip-address=192.168.1.18":

      $resultString = "";
      break;

    // provocated fails

    case "/etc/config-tools/timeout":

      sleep(2);
      $resultString = "output after long time";
      break;

    case "/etc/config-tools/empty_string":

       $resultString = "";
       break;

     case "/etc/config-tools/time":

       $resultString = sprintf("%d", time());
       break;

    case "/etc/config-tools/no_jason_return":

      echo "bla";
      die();

    case "/etc/config-tools/get_touchscreen_config display-state":

      $resultString = "off";
      break;

    default: $status = -1;
  }

  return $resultString;
}

function AccessAllowed($configtool, $username = "")
{
  // userlevel => configtool names
  $accessRights = array(

    USERLEVEL_ADMIN => array(
        'get_user_info',
        'get_wbm_uploads',
        'get_snmp_data',

        'config_snmp',

        'start_reboot',
        'config_user',
        'config_RS232',
        'copy_firmware_image',
        'switch_bootloader',
        'fat_format',
        'firmware_backup',
        'firmware_restore',
        'activate_download',
        'config_service_interface',
        'get_service_interface_config',
        'config_opcua',
        'config-opcua',
        'get_dataagent_config',
        'config_dataagent',
        'config_routing'
        ),

    USERLEVEL_USER  => array(
        'get_telecontrol_state',
        'get_rts3scfg_value',
        'get_default_gateway_config',
        'get_dns_server',
        'get_dns_service_config',
        'get_rts3scfg_value',
        'get_filesystem_data',
        'get_firmware_restore_packages',
        'get_port_state',
        'get_iocheckport_config',
        'config_iocheckport',
        'config_ssl',
        'get_ntp_config',
        'get_ssh_config',
        'get_tftp_config',
        'get_dhcpd_config',
        'pbdp_config',
        'get_switch_settings',

        'config_plc',
        'config_port',
        'change_rts_config',
        'config_dns_service',
        'config_runtime',
        'config_linux_user',
        'config_interfaces',
        'set_dsa_mode',
        'config_default_gateway',
        'config_switch',
        'config_ethernet',
        'edit_dns_server',
        'firewall',
        'firewall_apply.sh',
        'config_clock',
        'config_clock_display_mode',
        'config_timezone',
        'config_port',
        'config_sntp',
        'config_ssh',
        'config_tftp',
        'config_dhcpd',
        'config_mdmd',
        'modbus_config',

        'restart_webserver'
        ),

    USERLEVEL_GUEST => array(
        'get_coupler_details',
        'get_typelabel_value',
        'get_runtime_config',
        'get_clock_data',
        'get_eth_config',
        'get_actual_eth_config',
        'get_plc_config',
        'get_run_stop_switch_value',
        'get_led_config',
        'get_rts_info',
        'get_port_state',
        'get_wbm_diaglist',
        'get_possible_runtimes',
        'detectfeature'
        )
  );

  $accessAllowed = false;

  switch($username)
  {
    case 'user':   $userlevel = USERLEVEL_USER; break;
    case 'admin':  $userlevel = USERLEVEL_ADMIN; break;
    default:       $userlevel = USERLEVEL_GUEST; break;
  }

  if((USERLEVEL_GUEST <= $userlevel) && in_array($configtool, $accessRights[USERLEVEL_GUEST]))
  {
    $accessAllowed = true;
  }
  else if((USERLEVEL_USER <= $userlevel) && in_array($configtool, $accessRights[USERLEVEL_USER]))
  {
    $accessAllowed = true;
  }

  // access for admin always allowed
  else if((USERLEVEL_ADMIN <= $userlevel)) // && in_array($configtool, $accessRights[USERLEVEL_ADMIN]))
  {
    $accessAllowed = true;
  }

  return $accessAllowed;
}


function ConfigtoolNameInvalid($name)
{
  $nameInvalid = false;

  if(strpos($name, "/"))
  {
    $nameInvalid = true;
  }

  return $nameInvalid;
}

function CallConfigtool($configtoolObj, &$resultObj, $username = "", $cmdStatus )
{
  //$status       = $cmdStatus;
  $status       = SUCCESS;
  $resultString = "";
  $errorText    = "";
  $callString   = "";
  $dataId       = isset($configtoolObj["dataId"]) ? $configtoolObj["dataId"] : "";

  /*
  if(SUCCESS != $status)
  {
    GetErrorTxt($status, $errorText);
  }
  else if(!isset($configtoolObj))
  */
  //echo $cmdStatus;
  if(!isset($configtoolObj))
  {
    $status     = ERROR;
    $errorText  = "Missing input";
  }

  else if(ConfigtoolNameInvalid($configtoolObj["name"]))
  {
    $status     = ERROR;
    $errorText  = "Invalid configtool name (".$configtoolObj["name"].")";
  }

  else if(!file_exists(sprintf("/etc/config-tools/%s", $configtoolObj["name"])))
  {
    $status     = ERROR;
    $errorText  = "Unknown configtool (".$configtoolObj["name"].")";
  }

  // check if access is allowed in related to user level
  else if(!AccessAllowed($configtoolObj["name"], $username))
  {
    $status     = ACCESS_NOT_ALLOWED;
    $errorText  = "Access not allowed.";
  }

  // check, if cfrs token is ok
  // NOTE: actually, user "guest" (= username is empty, no session is existing) is allowed to read a few params, so
  // we don't check crsr here, if access was basically allowed for this configtool
  // we will change that later on in case of stronger security configuration
  else if(strlen($username) && (SUCCESS != $cmdStatus))
  {
    $status    = $cmdStatus;
    $errorText = Session_GetErrorTxt($status);
    //$errorText = $errorText.$username.$cmdStatus;
  }
  else
  {
    // by now, generally call all configtools with sudo
    $callString = "sudo ";

    // create string to call configtool by linux shell - first directory and configtool name
    $callString = $callString."/etc/config-tools/".$configtoolObj["name"];

    // now all configtool parameters, one after the other
    $paramNo = 0;
    while(isset($configtoolObj["parameter"][$paramNo]))
    {
      // decode parameter in url format (to save spaces eg.), but '=' (changed to "%3d") must be decoded again
      /*
      $paramString = urlencode($configtoolObj["parameter"][$paramNo]);
      $paramString = str_replace("%3D", "=", $paramString);
      $paramString = str_replace("%3A", ":", $paramString);
      $paramString = str_replace("%2F", "/", $paramString);
      */

      // decode '%' and '+', because they have special meanings in the encoded characters
      $paramString = $configtoolObj["parameter"][$paramNo];
      $paramString = str_replace("%", "%25", $paramString);
      $paramString = str_replace("+", "%2b", $paramString);

      // decode characters in url format, which otherwise confuse the bash
      $paramString = str_replace(" ", "%20", $paramString);
      $paramString = str_replace("'", "%27", $paramString);
      $paramString = str_replace('"', "%22", $paramString);
      $paramString = str_replace('`', "%60", $paramString);
      $paramString = str_replace('*', "%2a", $paramString);

      // escape parameter string for bash
      // (add (single!) quotation marks around parameter string to let the bash don't interprete "(", ")", and so on
      // and mask quotation mark itself inside string)
      // "escapeshellarg" does not work, because it strippes UTF8 characters from string and setlocale does not help
      $paramString = "'".str_replace("'", "'\\''", $paramString)."'";

      // add parameter to call string
      $callString = $callString." ".$paramString;

      ++$paramNo;
    }

    $dummyOutput;

    // unit test is executed - get specified test output
    if(MOCK) $resultString = MockConfigtool($callString, $dummyOutput, $status);

    // multiline output expected - use special shell call, which transfers more than one output line
    else if(isset($configtoolObj["multiline"]) && $configtoolObj["multiline"])
    {
      $resultString = shell_exec($callString);
    }

    // normal call of configtool
    else $resultString = exec($callString, $dummyOutput, $status);

    if($resultString == NULL) $resultString = "";

    if(0 != $status)
    {
      $lastErrorFilename = "/tmp/last_error.txt";
      if(file_exists($lastErrorFilename))
      {
        $errorText = file_get_contents($lastErrorFilename);
        //unlink($lastErrorFilename);
        exec(sprintf("sudo /bin/rm %s", $lastErrorFilename));
      }
    }
  }

  // to return call string (only!) for debugging, comment out next line meanwhile
  $callString = "";

  $resultObj = array('status'       => $status,
                     'resultString' => $resultString,
                     'errorText'    => htmlspecialchars($errorText, ENT_QUOTES),
                     'dataId'       => htmlspecialchars($dataId, ENT_QUOTES),
                     'callString'   => htmlspecialchars($callString, ENT_QUOTES) );
}



function CallConfigtoolByJson()
{
  // get request json string by stdin
  $requestString    = file_get_contents("php://input");
  $username         = "";
  $csrfId = "";
  $cmdStatus = "";
  //echo "requestString:".$requestString;

  if($requestString)
  {
    $configtoolNo = 0;
    // decode string to php array/object
    $aRequest = json_decode($requestString, true);

    //var_dump($aRequest);
    if(isset($aRequest["csrfToken"]))
    {
      $csrfId = htmlspecialchars($aRequest["csrfToken"], ENT_QUOTES);
      $aDeviceParams = isset($aRequest["aDeviceParams"]) ? $aRequest["aDeviceParams"] : array();
      // cyclic clock_data command has to be ignored because of session lifetime
      $cmdStatus = Session_HandleSessionLifeTime($csrfId, $aRequest["renewSession"]);
    }
    else
    {
      //$aDeviceParams = isset($aRequest["aDeviceParams"]) ? $aRequest["aDeviceParams"] : array();
    }

    $username = isset($_SESSION["username"]) ? $_SESSION["username"] : "";

    while(isset($aDeviceParams[$configtoolNo]))
    {
      CallConfigtool($aDeviceParams[$configtoolNo], $aDeviceResponse[$configtoolNo], $username, $cmdStatus);
      ++$configtoolNo;
    }
  }

  if(isset($aRequest["csrfToken"]))
  {
    //test_csrf
    $aResponse  = array("csrfToken" => $csrfId, "aDeviceResponse" => $aDeviceResponse);
  }
  else
  {
    //$aResponse  = array("aDeviceResponse" => $aDeviceResponse);
    $aResponse = isset($aDeviceResponse) ? $aDeviceResponse : false;
  }

  // encode responses to json string and send it to stdout
  //var_dump($aResponse);
  echo json_encode($aResponse);
}

// for session lifetime
function isCycClockCommand($aDeviceParams)
{
  // pattern for controller infos (cyclic command pattern)
  $cycClockCmds = array('get_clock_data','get_clock_data','get_run_stop_switch_value','get_led_config');
  $cycClockCmdMax = count($cycClockCmds);
  $cycClockCmdCount = 0;
  $isCycClockCmd = false;
  while(isset($aDeviceParams[$cycClockCmdCount]))
  {
    $configToolObj = $aDeviceParams[$cycClockCmdCount];
    if($cycClockCmdMax > $cycClockCmdCount)
    {
      if($configToolObj["name"] == $cycClockCmds[$cycClockCmdCount])
      {
        ++$cycClockCmdCount;
      }
      else
        break;
    }
    else
    {
      $cycClockCmdCount = 0;
      break;
    }
  }

  if($cycClockCmdMax == $cycClockCmdCount)
    $isCycClockCmd = true;

  return $isCycClockCmd;
}

CallConfigtoolByJson();

?>
