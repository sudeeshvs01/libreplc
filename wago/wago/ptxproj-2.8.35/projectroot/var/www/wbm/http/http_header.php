<?php namespace http;


function request_header($name) {
    $name = strtoupper($name);
    $name = str_replace('-', '_', $name);
    $name = "HTTP_$name";
    return $_SERVER[$name];
}
